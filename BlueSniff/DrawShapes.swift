//
//  DrawShapes.swift
//  BlueSniff
//
//  Created by Demetres Stordopoulos on 07/04/16.
//  Copyright © 2016 Tribe Wearables. All rights reserved.
//

import Foundation
import UIKit

class DrawShapes {
    
    
    
    private func createLabelWithAttributes(text: String,
                                           labelFrame: CGRect,
                                           centerAt: CGPoint,
                                           fontSize: CGFloat
        ) -> UILabel {
        // Create label
        let muscleLabel = UILabel(frame: labelFrame)
        
        // Coordinate location and labelFrame
        //muscleLabel.center = dimentions
        muscleLabel.center = centerAt
        muscleLabel.textAlignment = .Center
        muscleLabel.attributedText = NSMutableAttributedString(string: text, attributes: [NSFontAttributeName:UIFont(name: "muscleShapesFont", size: fontSize)!])
        
        return muscleLabel
    }
    
    func frontBody(withMainView: UIView) -> (view: UIView, labels: [UILabel]) {
        
        var labelArray: [UILabel] = []
        
        // Center view
        let heightOfView = CGFloat(410)
        let viewY: CGFloat = withMainView.frame.size.height/2 - heightOfView/2
        
        // make view for half the front body muscles
        let halfFrontBodyMuscles = UIView(frame: CGRectMake(0, viewY, 145, heightOfView))
        
        // Create original muscle labels
        let rightBicepFront     = createLabelWithAttributes("A", labelFrame: CGRectMake(0, 0, 64, 64), centerAt: CGPointMake(20, 89), fontSize: 48)
        let rightDeltoidFront   = createLabelWithAttributes("B", labelFrame: CGRectMake(0, 0, 64, 64), centerAt: CGPointMake(30, 49), fontSize: 32)
        let rightChestFront     = createLabelWithAttributes("C", labelFrame: CGRectMake(0, 0, 64, 64), centerAt: CGPointMake(58, 56), fontSize: 48)
        let rightTrapeziusFront = createLabelWithAttributes("D", labelFrame: CGRectMake(0, 0, 64, 64), centerAt: CGPointMake(59, 20), fontSize: 20)
        let rightAbs            = createLabelWithAttributes("E", labelFrame: CGRectMake(0, 0, 64, 158), centerAt: CGPointMake(51, 140), fontSize: 110)
        let rightAbductorFront  = createLabelWithAttributes("F", labelFrame: CGRectMake(0, 0, 93, 229), centerAt: CGPointMake(38, 225), fontSize: 110)
        let rightQuadricepFront = createLabelWithAttributes("G", labelFrame: CGRectMake(0, 0, 93, 229), centerAt: CGPointMake(48, 240), fontSize: 130)
        
        // add original labels to the label array
        labelArray.append(rightBicepFront)
        labelArray.append(rightDeltoidFront)
        labelArray.append(rightChestFront)
        labelArray.append(rightTrapeziusFront)
        labelArray.append(rightAbs)
        labelArray.append(rightAbductorFront)
        labelArray.append(rightQuadricepFront)
        
        // Mirror all the muscles on the left
        let viewPaddingDistance: CGFloat = 11
        let widthMirrorDistance = withMainView.frame.size.width/2 + viewPaddingDistance
        
        // Create mirror labels with exact the same properties as the original onces but widthMirrorDistance away
        let leftBicepFront = createLabelWithAttributes("A", labelFrame: CGRectMake(0, 0, 64, 64), centerAt: CGPointMake(widthMirrorDistance-20, 89), fontSize: 48)
        let leftDeltoidFront   = createLabelWithAttributes("B", labelFrame: CGRectMake(0, 0, 64, 64), centerAt: CGPointMake(widthMirrorDistance-30, 49), fontSize: 32)
        let leftChestFront     = createLabelWithAttributes("C", labelFrame: CGRectMake(0, 0, 64, 64), centerAt: CGPointMake(widthMirrorDistance-58, 56), fontSize: 48)
        let leftTrapeziusFront = createLabelWithAttributes("D", labelFrame: CGRectMake(0, 0, 64, 64), centerAt: CGPointMake(widthMirrorDistance-59, 20), fontSize: 20)
        let leftAbs            = createLabelWithAttributes("E", labelFrame: CGRectMake(0, 0, 64, 158), centerAt: CGPointMake(widthMirrorDistance-51, 140), fontSize: 110)
        let leftAbductorFront  = createLabelWithAttributes("F", labelFrame: CGRectMake(0, 0, 93, 229), centerAt: CGPointMake(widthMirrorDistance-38, 225), fontSize: 110)
        let leftQuadricepFront = createLabelWithAttributes("G", labelFrame: CGRectMake(0, 0, 93, 229), centerAt: CGPointMake(widthMirrorDistance-48, 240), fontSize: 130)
        
        // add mirrored labels to the label array
        labelArray.append(leftBicepFront)
        labelArray.append(leftDeltoidFront)
        labelArray.append(leftChestFront)
        labelArray.append(leftTrapeziusFront)
        labelArray.append(leftAbs)
        labelArray.append(leftAbductorFront)
        labelArray.append(leftQuadricepFront)
        
        // Loop every label in labelArray
        for (index, addedLabel) in labelArray.enumerate() {
            // Mirror half the labels
            if index > labelArray.count/2-1 {
                addedLabel.transform = CGAffineTransformMakeScale(-1, 1)
            }
            
            // Add every label to the halfFrontBodyMuscles view
            halfFrontBodyMuscles.addSubview(addedLabel)
        }
        
        
        // Scale Image to shapeScale
        let shapeScale: CGFloat = 1
        halfFrontBodyMuscles.transform = CGAffineTransformMakeScale(shapeScale , shapeScale)
        
        return (halfFrontBodyMuscles, labelArray)
    }
    
    func backBody(withMainView: UIView) -> (view: UIView, labels: [UILabel]) {
        
        var labelArray: [UILabel] = []
        
        let widthOfBackMusclesView: CGFloat = 145
        let horrizontalPosiitonOfBackMusclesView = withMainView.frame.size.width - widthOfBackMusclesView - 10
        
        // Center view
        let heightOfBackMusclesView: CGFloat = 410
        let viewY: CGFloat = withMainView.frame.size.height/2 - heightOfBackMusclesView/2
        
        // make view for half the front body muscles
        let halfFrontBodyMuscles = UIView(frame: CGRectMake(horrizontalPosiitonOfBackMusclesView, viewY, widthOfBackMusclesView, heightOfBackMusclesView))
        
        // Create original muscle labels
        let leftTricepBack     = createLabelWithAttributes("O", labelFrame: CGRectMake(0, 0, 64, 64), centerAt: CGPointMake(20, 89), fontSize: 52)
        let leftDeltoidBack   = createLabelWithAttributes("P", labelFrame: CGRectMake(0, 0, 64, 64), centerAt: CGPointMake(30, 49), fontSize: 42)
        let leftTrapeziusBack     = createLabelWithAttributes("Q", labelFrame: CGRectMake(0, 0, 64, 64), centerAt: CGPointMake(54, 54), fontSize: 55)
        let leftBack = createLabelWithAttributes("R", labelFrame: CGRectMake(0, 0, 64, 64), centerAt: CGPointMake(47, 100), fontSize: 76)
        let leftGlutes            = createLabelWithAttributes("S", labelFrame: CGRectMake(0, 0, 64, 158), centerAt: CGPointMake(52, 195), fontSize: 63)
        let leftHumstring  = createLabelWithAttributes("T", labelFrame: CGRectMake(0, 0, 93, 229), centerAt: CGPointMake(50, 262), fontSize: 110)
        
        // add original labels to the label array
        labelArray.append(leftBack)
        labelArray.append(leftTricepBack)
        labelArray.append(leftDeltoidBack)
        labelArray.append(leftTrapeziusBack)
        
        labelArray.append(leftGlutes)
        labelArray.append(leftHumstring)
        
        // Mirror all the muscles on the left
        let viewPaddingDistance: CGFloat = -3
        let widthMirrorDistance = withMainView.frame.size.width/2 + viewPaddingDistance
        
        // Create mirror labels with exact the same properties as the original onces but widthMirrorDistance away
        let rightTricepBack     = createLabelWithAttributes("O", labelFrame: CGRectMake(0, 0, 64, 64), centerAt: CGPointMake(widthMirrorDistance-20, 89), fontSize: 52)
        let rightDeltoidBack   = createLabelWithAttributes("P", labelFrame: CGRectMake(0, 0, 64, 64), centerAt: CGPointMake(widthMirrorDistance-30, 49), fontSize: 42)
        let rightTrapeziusBack     = createLabelWithAttributes("Q", labelFrame: CGRectMake(0, 0, 64, 64), centerAt: CGPointMake(widthMirrorDistance-54, 54), fontSize: 55)
        let rightBack = createLabelWithAttributes("R", labelFrame: CGRectMake(0, 0, 64, 64), centerAt: CGPointMake(widthMirrorDistance-47, 100), fontSize: 76)
        let rightGlutes            = createLabelWithAttributes("S", labelFrame: CGRectMake(0, 0, 64, 158), centerAt: CGPointMake(widthMirrorDistance-52, 195), fontSize: 63)
        let rightHumstring  = createLabelWithAttributes("T", labelFrame: CGRectMake(0, 0, 93, 229), centerAt: CGPointMake(widthMirrorDistance-50, 262), fontSize: 110)
        
        // add mirrored labels to the label array
        labelArray.append(rightTricepBack)
        labelArray.append(rightDeltoidBack)
        labelArray.append(rightTrapeziusBack)
        labelArray.append(rightBack)
        labelArray.append(rightGlutes)
        labelArray.append(rightHumstring)
        
        // Loop every label in labelArray
        for (index, addedLabel) in labelArray.enumerate() {
            // Mirror half the labels
            if index > labelArray.count/2-1 {
                addedLabel.transform = CGAffineTransformMakeScale(-1, 1)
            }
            
            // Add every label to the halfFrontBodyMuscles view
            halfFrontBodyMuscles.addSubview(addedLabel)
        }
        
        
        // Scale Image to shapeScale
        let shapeScale: CGFloat = 1
        halfFrontBodyMuscles.transform = CGAffineTransformMakeScale(shapeScale , shapeScale)
        
        return (halfFrontBodyMuscles, labelArray)
    }
    
}