//
//  ColorManipulation.swift
//  BlueSniff
//
//  Created by Demetres Stordopoulos on 19/05/16.
//  Copyright © 2016 Tribe Wearables. All rights reserved.
//

import Foundation
import UIKit

class ColorManipulation {
    
    private func changeColorFrom(from: CGFloat, to: CGFloat, withStep: CGFloat) -> CGFloat {
        let step = abs(from-to)/withStep
        let newColor: CGFloat = from > to ? from - step : from + step
        return newColor
    }
    
    private func extractColorsFromUIColor(color: UIColor) -> [CGFloat] {
        var red: CGFloat = 0, green: CGFloat = 0, blue: CGFloat = 0, alpha: CGFloat = 0
        color.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        return [red, green, blue, alpha]
    }
    
    private func changeUIColorFrom(from: UIColor, to: UIColor, withStep: CGFloat) -> UIColor {
        var newUIColor: UIColor
        
        // var (fromRed, fromGreen, fromBlue, fromAlpha) = extractColorsFromUIColor(from)
        
        let fromExtractedColor = extractColorsFromUIColor(from)
        let toExtractedColor = extractColorsFromUIColor(to)
        
        var newUIColorArray: [CGFloat] = []
        
        for loopedColor in 0..<4 {
            newUIColorArray.append( changeColorFrom(fromExtractedColor[loopedColor], to: toExtractedColor[loopedColor], withStep: withStep) )
        }
        
        newUIColor = UIColor(red: newUIColorArray[0], green: newUIColorArray[1], blue: newUIColorArray[2], alpha: newUIColorArray[3])
        
        return newUIColor
    }
    
    // Generate a colorList containing UIColors with colorList.count == maxIntensity.
    func generateUIColorArrayForMaximumIntensityOf(maxIntensity: UInt16) -> [UIColor] {
        
        let black = UIColor(red:0.35, green:0.35, blue:0.35, alpha:1.0)     // black: 585858
        let blue = UIColor(red:0.31, green:0.60, blue:0.87, alpha:1.0)      // blue: 4e98de
        let green = UIColor(red:0.52, green:0.89, blue:0.45, alpha:1.0)     // green: 77df5b
        let orange = UIColor(red:1.00, green:0.53, blue:0.35, alpha:1.0)    // orange: ff885a
        let red = UIColor(red:0.96, green:0.34, blue:0.40, alpha:1.0)       // red: f55666
        
        // Main UIColors representing the muscle's state
        let colorList = [black, blue, green, orange, red]
        
        // List of colors, containing the colorList UIColors and programatically genereted UIColors in between them.
        var newColorList: [UIColor] = []
        
        // How many steps does a color change require between two colors
        let stepsBetweenColors = CGFloat(maxIntensity/UInt16(colorList.count)-1)
        
        // Initiate a color to use it for color generation
        var startColor: UIColor = changeUIColorFrom(blue, to: green, withStep: stepsBetweenColors)
        
        // iterate through all the main UIColors in colorList
        for destinationColor in colorList {
            // create one color for every step in between every two UIColors in the colorList
            for _ in 0..<Int(stepsBetweenColors) {
                startColor = changeUIColorFrom(startColor, to: destinationColor, withStep: stepsBetweenColors)
                newColorList.append(startColor)
            }
        }
        
        return newColorList
    }

    
}