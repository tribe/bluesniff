//
//  ViewController.swift
//  BlueSniff
//
//  Created by Demetres Stordopoulos on 31/03/16.
//  Copyright © 2016 Tribe Wearables. All rights reserved.
//

import UIKit
import CoreBluetooth


class ViewController: UIViewController, CBCentralManagerDelegate, CBPeripheralDelegate {
    
    // Name of the device we are searching for
    let deviceName: String = "WISH v0.4.2"// "WISH v0.4.2"
    
    // Font variables
    let muscleFontTest = "ABCDE"
    var muscleFontMutableString = NSMutableAttributedString()
    
    // Data Processing Thread
    let backgroundQueue: dispatch_queue_t = dispatch_queue_create("com.tribewearables.Calculations", DISPATCH_QUEUE_CONCURRENT)
    
    // BLE variables
    var centralManager = CBCentralManager?()
    var tribePeripheral : CBPeripheral!
    
    // Wish Services UUIDs
    let wishEMGServiceUUID = CBUUID(string: "4444")
    let wishEMGDataUUID = CBUUID(string: "4441")
    let wishEMGIConfigUUID = CBUUID(string: "0000")
    
    // EMG data labels
    @IBOutlet weak var emgIntencityLabel: UILabel!
    @IBOutlet weak var fontTestingLabel: UILabel!
    
    // EMG visual data labels
    @IBOutlet weak var rightBicepFront: UILabel!
    @IBOutlet weak var rightDeltoidFront: UILabel!
    @IBOutlet weak var rightTrapeziusFront: UILabel!
    @IBOutlet weak var rightChestFront: UILabel!
    @IBOutlet weak var rightAbs: UILabel!
    @IBOutlet weak var rightAbducentFront: UILabel!
    @IBOutlet weak var rightQuadricepFront: UILabel!
    
    private var fullBodyMusclesLabels:[Muscles] = []
    private var shortsMusclesLabels:[UILabel] = []
    private var shirtMusclesLabels:[UILabel] = []
    
    let muscleColorArray = ColorManipulation().generateUIColorArrayForMaximumIntensityOf(70)
    
    struct Muscles {
        let indexAtEMGSignal: Int
        let uiElement: UILabel
        var maxIntensity: UInt16
        private var colorMap: [UIColor]
        
        mutating func setMaxIntensity(newMaxIntensity: UInt16) {
            maxIntensity = newMaxIntensity
            colorMap = ColorManipulation().generateUIColorArrayForMaximumIntensityOf(newMaxIntensity)
        }
        
        init (_indexAtEMGSignal: Int, _uiElement: UILabel, _maxIntensity: UInt16) {
            indexAtEMGSignal = _indexAtEMGSignal
            uiElement = _uiElement
            maxIntensity = _maxIntensity
            colorMap = ColorManipulation().generateUIColorArrayForMaximumIntensityOf(_maxIntensity)
        }
        
        func getColorForIntensity(muscleIntensity: UInt16) -> UIColor {
            return muscleIntensity < maxIntensity ? colorMap[Int(muscleIntensity)] : colorMap.last!
        }
    }
    
    var shortsMuscles: [Muscles] = []
    var shirtMuscles: [Muscles] = []
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Change color of fontTestingLabel letters
        
        let muscleViewData = DrawShapes().frontBody(self.view)
        
        let muscleViewDataBack = DrawShapes().backBody(self.view)
        
        self.view.addSubview(muscleViewData.view)
        self.view.addSubview(muscleViewDataBack.view)
        
        
        // fullBodyMusclesLabels = muscleViewData.labels + muscleViewDataBack.labels
        
        /*
         // add original labels to the label array
         0 labelArray.append(rightBicepFront)
         1 labelArray.append(rightDeltoidFront)
         2 labelArray.append(rightChestFront)
         3 labelArray.append(rightTrapeziusFront)
         4 labelArray.append(rightAbs)
         - 5 labelArray.append(rightAbductorFront)
         - 6 labelArray.append(rightQuadricepFront)
        
         // add mirrored labels to the label array
         7 labelArray.append(leftBicepFront)
         8 labelArray.append(leftDeltoidFront)
         9 labelArray.append(leftChestFront)
         10 labelArray.append(leftTrapeziusFront)
         11 labelArray.append(leftAbs)
         - 12 labelArray.append(leftAbductorFront)
         - 13 labelArray.append(leftQuadricepFront)
        
        0 labelArray.append(leftBack)
        1 labelArray.append(leftTricepBack)
        2 labelArray.append(leftDeltoidBack)
        3 labelArray.append(leftTrapeziusBack)
        - 4 labelArray.append(leftGlutesBack)
        - 5 labelArray.append(leftHumstringBack)
        
         // add mirrored labels to the label array
         6 labelArray.append(rightTricepBack)
         7 labelArray.append(rightDeltoidBack)
         8 labelArray.append(rightTrapeziusBack)
         9 labelArray.append(rightBack)
         - 10 labelArray.append(rightGlutesBack)
         - 11 labelArray.append(rightHumstringBack)
        */
        
        
        /* Shorts Muscles Lables
        shortsMusclesLabels.append(muscleViewDataBack.labels[10]) // rightGlutesBack
        shortsMusclesLabels.append(muscleViewDataBack.labels[11]) // rightHumstringBack
        shortsMusclesLabels.append(muscleViewData.labels[6]) // rightQuadricepFront
        shortsMusclesLabels.append(muscleViewData.labels[13]) // leftQuadricepFront
        shortsMusclesLabels.append(muscleViewData.labels[5]) // rightAbductorFront
        shortsMusclesLabels.append(muscleViewData.labels[12]) // leftAbductorFront
        shortsMusclesLabels.append(muscleViewDataBack.labels[4]) // leftGlutesBack
        shortsMusclesLabels.append(muscleViewDataBack.labels[5]) // leftHumstringBack
        */
        
        
        // rightGlutesBack
        shortsMuscles.append(Muscles(   _indexAtEMGSignal: 0,
                                        _uiElement: muscleViewDataBack.labels[10],
                                        _maxIntensity: 70)
        )
        
        // rightHumstringBack
        shortsMuscles.append(Muscles(   _indexAtEMGSignal: 0,
                                        _uiElement: muscleViewDataBack.labels[11],
                                        _maxIntensity: 70)
        )
        
        // rightQuadricepFront
        shortsMuscles.append(Muscles(   _indexAtEMGSignal: 0,
                                        _uiElement: muscleViewData.labels[6],
                                        _maxIntensity: 70)
        )
        
        // leftQuadricepFront
        shortsMuscles.append(Muscles(   _indexAtEMGSignal: 0,
                                        _uiElement: muscleViewData.labels[13],
                                        _maxIntensity: 70)
        )
        
        // rightAbductorFront
        shortsMuscles.append(Muscles(   _indexAtEMGSignal: 0,
                                        _uiElement: muscleViewData.labels[5],
                                        _maxIntensity: 70)
        )
        
        // leftAbductorFront
        shortsMuscles.append(Muscles(   _indexAtEMGSignal: 0,
                                        _uiElement: muscleViewData.labels[12],
                                        _maxIntensity: 70)
        )
        
        // leftGlutesBack
        shortsMuscles.append(Muscles(   _indexAtEMGSignal: 0,
                                        _uiElement: muscleViewDataBack.labels[4],
                                        _maxIntensity: 70)
        )
        
        // leftHumstringBack
        shortsMuscles.append(Muscles(   _indexAtEMGSignal: 0,
                                        _uiElement: muscleViewDataBack.labels[5],
                                        _maxIntensity: 70)
        )
        
        
        // Shirt Muscles Lables
        
        
        // rightBicepFront
        shirtMuscles.append(Muscles(   _indexAtEMGSignal: 0,
            _uiElement: muscleViewData.labels[0],
            _maxIntensity: 70)
        )
        
        // rightDeltoidFront
        shirtMuscles.append(Muscles(   _indexAtEMGSignal: 0,
            _uiElement: muscleViewData.labels[1],
            _maxIntensity: 70)
        )

        // rightChestFront
        shirtMuscles.append(Muscles(   _indexAtEMGSignal: 0,
            _uiElement: muscleViewData.labels[2],
            _maxIntensity: 70)
        )
        
        // rightTrapeziusFront
        shirtMuscles.append(Muscles(   _indexAtEMGSignal: 0,
            _uiElement: muscleViewData.labels[3],
            _maxIntensity: 70)
        )
        
        // rightAbs
        shirtMuscles.append(Muscles(   _indexAtEMGSignal: 0,
            _uiElement: muscleViewData.labels[4],
            _maxIntensity: 70)
        )
        
        // leftBicepFront
        shirtMuscles.append(Muscles(   _indexAtEMGSignal: 0,
            _uiElement: muscleViewData.labels[7],
            _maxIntensity: 70)
        )
        
        // leftDeltoidFront
        shirtMuscles.append(Muscles(   _indexAtEMGSignal: 0,
            _uiElement: muscleViewData.labels[8],
            _maxIntensity: 70)
        )
        
        // leftChestFront
        shirtMuscles.append(Muscles(   _indexAtEMGSignal: 0,
            _uiElement: muscleViewData.labels[9],
            _maxIntensity: 70)
        )
        
        // leftTrapeziusFront
        shirtMuscles.append(Muscles(   _indexAtEMGSignal: 0,
            _uiElement: muscleViewData.labels[10],
            _maxIntensity: 70)
        )
        
        // leftAbs
        shirtMuscles.append(Muscles(   _indexAtEMGSignal: 0,
            _uiElement: muscleViewData.labels[11],
            _maxIntensity: 70)
        )
        
        // BACK muscles
        
        // leftBack
        shirtMuscles.append(Muscles(   _indexAtEMGSignal: 0,
            _uiElement: muscleViewDataBack.labels[0],
            _maxIntensity: 70)
        )
        
        // leftTricepBack
        shirtMuscles.append(Muscles(   _indexAtEMGSignal: 0,
            _uiElement: muscleViewDataBack.labels[1],
            _maxIntensity: 70)
        )
        
        // leftDeltoidBack
        shirtMuscles.append(Muscles(   _indexAtEMGSignal: 0,
            _uiElement: muscleViewDataBack.labels[2],
            _maxIntensity: 70)
        )
        
        // leftTrapeziusBack
        shirtMuscles.append(Muscles(   _indexAtEMGSignal: 0,
            _uiElement: muscleViewDataBack.labels[3],
            _maxIntensity: 70)
        )
        
        // rightTricepBack
        shirtMuscles.append(Muscles(   _indexAtEMGSignal: 0,
            _uiElement: muscleViewDataBack.labels[6],
            _maxIntensity: 70)
        )
        
        // rightDeltoidBack
        shirtMuscles.append(Muscles(   _indexAtEMGSignal: 0,
            _uiElement: muscleViewDataBack.labels[7],
            _maxIntensity: 70)
        )
        
        // rightTrapeziusBack
        shirtMuscles.append(Muscles(   _indexAtEMGSignal: 0,
            _uiElement: muscleViewDataBack.labels[8],
            _maxIntensity: 70)
        )
        
        // rightBack
        shirtMuscles.append(Muscles(   _indexAtEMGSignal: 0,
            _uiElement: muscleViewDataBack.labels[9],
            _maxIntensity: 70)
        )
        
        
        
        
        
        /* Shirt Muscles Lables
        shirtMusclesLabels.append(muscleViewData.labels[0])
        shirtMusclesLabels.append(muscleViewData.labels[1])
        shirtMusclesLabels.append(muscleViewData.labels[2])
        shirtMusclesLabels.append(muscleViewData.labels[3])
        shirtMusclesLabels.append(muscleViewData.labels[4])
        shirtMusclesLabels.append(muscleViewData.labels[7])
        shirtMusclesLabels.append(muscleViewData.labels[8])
        shirtMusclesLabels.append(muscleViewData.labels[9])
        shirtMusclesLabels.append(muscleViewData.labels[10])
        shirtMusclesLabels.append(muscleViewData.labels[11])
        
        shirtMusclesLabels.append(muscleViewDataBack.labels[0])
        shirtMusclesLabels.append(muscleViewDataBack.labels[1])
        shirtMusclesLabels.append(muscleViewDataBack.labels[2])
        shirtMusclesLabels.append(muscleViewDataBack.labels[3])
        shirtMusclesLabels.append(muscleViewDataBack.labels[6])
        shirtMusclesLabels.append(muscleViewDataBack.labels[7])
        shirtMusclesLabels.append(muscleViewDataBack.labels[8])
        shirtMusclesLabels.append(muscleViewDataBack.labels[9])
        
        
        fullBodyMusclesLabels = shortsMusclesLabels + shirtMusclesLabels
        */
        
        fullBodyMusclesLabels = shortsMuscles + shirtMuscles
        
        
        // initialise serial queue for the Bluetooth to run on.
        let bleQueue: dispatch_queue_t = dispatch_queue_create("com.tribewearables.Bluetooth", DISPATCH_QUEUE_SERIAL);
        
        // As soon as the view loads up, initialize centralManager
        self.centralManager = CBCentralManager(delegate: self, queue: bleQueue)
        
    }
    
// MARK: CBCentralManagerDelegate
    
    func startScan() {
        // start scanning nearby devices
        // To scan for a spesific devices/UUIDs put a UUID list instead of the first argument: nil.
        // options: are the restrictions/limitations on the type of devices the centralManager will be scanning for
        self.centralManager?.scanForPeripheralsWithServices(nil, options: nil)
    }
    
    // didDiscoverPeripheral: called every time a peripheral is found
    func centralManager(central: CBCentralManager, didDiscoverPeripheral peripheral: CBPeripheral, advertisementData: [String : AnyObject], RSSI: NSNumber) {
        
        //NSLog("\n\n\nBLE device found \nName: \(peripheral.name) \nUUID: \(peripheral.identifier.UUIDString) \nAdvertisement Data: \(advertisementData) \nSignal Strength (RSSI): \(RSSI)\n\n\n")
        
        
        let nameOfDeviceFound = (advertisementData as NSDictionary).objectForKey(CBAdvertisementDataLocalNameKey) as? NSString
        
        if (nameOfDeviceFound != nil) {
            NSLog("Devices found: \(nameOfDeviceFound!)")
        }
        
        
        // If a device with deviceName is found, execute this
        if nameOfDeviceFound == deviceName {
            
            NSLog("\(nameOfDeviceFound!) Found")
            
            // Stop scanning. Since we have found the device scanning for other devices isn't useful.
            self.centralManager?.stopScan()
            
            // Set as the peripheral to use and establish connection
            self.tribePeripheral = peripheral
            self.tribePeripheral.delegate = self
            self.centralManager?.connectPeripheral(peripheral, options: nil)
        }
        else {
            NSLog("Tribe Wearables not found")
        }
    }
    
    /**
     The delegate method didConnectPeripheral gets called when a successful connection with the peripheral is established. On connection, we will call the peripheral method discoverServices() and update the statusLabel text as shown below.
     
     :param: central     the CBCentralManager (iOS Device)
     :param: peripheral   each peripheral that has been discovered.
     */
    func centralManager(central: CBCentralManager, didConnectPeripheral peripheral: CBPeripheral) {
        NSLog("Discovering Peripheral services")
        peripheral.discoverServices(nil)
    }
    
    // If disconnected, start searching again
    func centralManager(central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: NSError?) {
        NSLog("Disconnected")
        central.scanForPeripheralsWithServices(nil, options: nil)
    }
    
// MARK: CBCentralPeripheralDelegate
    
    // Check if the service discovered is a valid WISH EMG Service
    func peripheral(peripheral: CBPeripheral, didDiscoverServices error: NSError?) {
        NSLog("Looking at peripheral's services")
        
        // look through all the services of the peripheral
        for service in peripheral.services! {
            let currentService = service as CBService
            if service.UUID == wishEMGServiceUUID {
                // Discover characteristics of Wish EMG Service
                peripheral.discoverCharacteristics(nil, forService: currentService)
                NSLog("Discovered Wish's EMG Characteristic")
            }
        }
    }
    
    func peripheral(peripheral: CBPeripheral, didDiscoverCharacteristicsForService service: CBService, error: NSError?) {
        NSLog("Enabling Sensors")
        
        for characteristic in service.characteristics! {
            let thisCharacteristic = characteristic as CBCharacteristic
            // Check for data of characteristic
            if thisCharacteristic.UUID == wishEMGDataUUID {
                // if the wishEMGDataUUID is found, enable the peripheral's Notification
                self.tribePeripheral.setNotifyValue(true, forCharacteristic: thisCharacteristic)
            }
        }
    }
    
    private func changeColorOfUIElement(elementToChangeColor: UILabel, withColor: UIColor) {
        
        self.muscleFontMutableString = NSMutableAttributedString(string: elementToChangeColor.text!, attributes: [NSFontAttributeName:UIFont(name: "muscleShapesFont", size: elementToChangeColor.font.pointSize)!])
        
        // Change the color of the correspondent muscle (UILabel)
        self.muscleFontMutableString.addAttribute(NSForegroundColorAttributeName, value: withColor, range: NSRange(location: 0 ,length:1))
        
        elementToChangeColor.attributedText = self.muscleFontMutableString
    }
    
    /**
     Turn Every UInt16 element in emgArray into an RGB value
     
     :param: emgArray     Key: represents EMG Sensors, Index: represents EMG Value
     */
    private func colorUIOF(muscleUIElements: [Muscles], withDeviceEMGData: [UInt16]) {
        var colorOfUIElement: UIColor
        var colorForIntensity: Int
        let numberOfColorsInMuscleArray = muscleColorArray.count
        var emgDataAtIndex: Int
        
        for atIndex in 0..<muscleUIElements.count {
            
            
            
            emgDataAtIndex = Int(withDeviceEMGData[ muscleUIElements[atIndex].indexAtEMGSignal ])
            
            
            colorForIntensity = emgDataAtIndex < numberOfColorsInMuscleArray ? numberOfColorsInMuscleArray / emgDataAtIndex: numberOfColorsInMuscleArray-1
            colorOfUIElement = muscleColorArray[colorForIntensity]
            
            changeColorOfUIElement(muscleUIElements[atIndex].uiElement, withColor: colorOfUIElement)
        }
    }
    
    // Actions for when there are updates values from the BLE devices.
    func peripheral(peripheral: CBPeripheral, didUpdateValueForCharacteristic characteristic: CBCharacteristic, error: NSError?) {
        
        if characteristic.UUID == wishEMGDataUUID {
            
            // Convert NSData to array of signed 16bit values
            let emgDataBytes = characteristic.value
            
            // the number of elements:
            let count = emgDataBytes!.length / sizeof(UInt16)
            
            // create array of appropriate length:
            var emgArray = [UInt16](count: count, repeatedValue: 0)
            
            // NSLog("emgDataBytes: \(emgDataBytes!)")
            
            
            // Copy bytes into array
            emgDataBytes!.getBytes(&emgArray, length:count * sizeof(UInt16))
            
            // NSLog("emgArray: \(emgArray)")
            
            // Dispace EMG to RGB calculations to the main threat
            dispatch_async(dispatch_get_main_queue()) {
                
                /* Each Key in emgArray represents a Sensor, is an EMG value into RGB values and set them to
                 * Each UInt16 element in emgArray reprisent the EMG value from that Sensor
                 */
                //self.getRGBValueForEMGData(emgArray)
                
                // shortsMusclesLabels & shirtMusclesLabels
                self.colorUIOF(self.shortsMuscles, withDeviceEMGData: emgArray)
                
                NSLog("\nemgArray: \(emgArray)\n\n")
                
                self.view.setNeedsDisplay()
                
            } // END: Dispatch dispatch_get_main_queue
        }
    }
    
    
    
// MARK: CBManager tasks.
    
    
    // Check if the iOS device Bluetooth is On.
    func centralManagerDidUpdateState(central: CBCentralManager) {
        // Check if the bluetooth of the iOS device is On
        if central.state == CBCentralManagerState.PoweredOn {
            startScan()
        } else {
            
            /*
             * CBCentralManager is running on the "com.tribewearables.Bluetooth" thread.
             * so we need to run the UI code back to the main threat.
             */
            dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                // Create a UI alert
                let alertVC = UIAlertController(title: "Bluetooth problem", message: "Make sure the Bluetooth of your iOS device is on.", preferredStyle: UIAlertControllerStyle.Alert)
                
                // Display the alert to the user
                self.presentViewController(alertVC, animated: true, completion: nil)
                
                // Create a button for the Alert box.
                let bleMessageActionAlert = UIAlertAction(title: "Okay", style: UIAlertActionStyle.Default, handler: { (action: UIAlertAction) -> Void in self.dismissViewControllerAnimated(true, completion: nil)
                })
                
                // Add the button "Okay" to the Alert box.
                alertVC.addAction(bleMessageActionAlert)
                
                print("Somthing is wrong you your iOS device's Bluetooth: \(central.state.rawValue)")
            }
            
        }
        
        
    }

}

